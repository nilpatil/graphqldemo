package com.graphql.app;

import javax.websocket.ClientEndpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookDetailsGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookDetailsGraphqlApplication.class, args);
	}

}
